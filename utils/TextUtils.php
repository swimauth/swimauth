<?php
/**
 * Created by PhpStorm.
 * User: iMCDb
 * Date: 10/18/15
 * Time: 3:16 AM
 */

namespace app\utils {

    define ("_CHARSET", "UTF-8");

    class TextUtils
    {
        public static $INSTANCE = null;
        public static $ALLOWED_TAGS = "<b><i><u><center><hr><br /><br><blockquote><ol><ul><li><img><strong><em><div><p>";
        // Added 3.3
        public static function nl2br2($string)
        {
            $string = str_replace(array("\r\n", "\r", "\n"), "<br />", $string);
            return $string;
        }

        // Sanitizes user input to help prevent XSS attacks
        public static function descript($text)
        {
            $text = str_replace("@#","",$text);
            $text = str_replace("@$@","",$text);
            
            // Convert problematic ascii characters to their true values
            $search = array("40", "41", "58", "65", "66", "67", "68", "69", "70",
                "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", "81",
                "82", "83", "84", "85", "86", "87", "88", "89", "90", "97", "98",
                "99", "100", "101", "102", "103", "104", "105", "106", "107",
                "108", "109", "110", "111", "112", "113", "114", "115", "116",
                "117", "118", "119", "120", "121", "122");

            $replace = array("(", ")", ":", "a", "b", "c", "d", "e", "f", "g", "h",
                "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u",
                "v", "w", "x", "y", "z", "a", "b", "c", "d", "e", "f", "g", "h",
                "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u",
                "v", "w", "x", "y", "z");

            $entities = count($search);

            for ($i = 0; $i < $entities; $i++) $text = preg_replace("#(&\#)(0*" . $search[$i] . "+);*#si", $replace[$i], $text);

            $text = preg_replace('/(\ )+/', ' ', $text);
            //$text = preg_replace("/(\r){2,}/", "\n\n", $text);
            //$text = preg_replace("/(\n){2,}/", "\n\n", $text);
            $text = preg_replace("/(\r?\n){2,}/", "\n\n", $text);
            $text = preg_replace('/(<br[^>]*>\s*){2,}/', '<br/><br/>', $text);

            $text = preg_replace('/\.{4,}/', '...', $text);
            $text = preg_replace('/\'{2}/', '"', $text);

            // the following is based on code from bitflux (http://blog.bitflux.ch/wiki/)
            // Kill hexadecimal characters completely
            $text = preg_replace('#(&\#x)([0-9A-F]+);*#si', "", $text);

            // remove any attribute starting with "on" or xmlns
            //https://s-media-cache-ak0.pinimg.com/736x/5a/2c/c8/5a2cc8863d0f6d586537f0c18a68dbc2.jpg
            $text = preg_replace('#(<[^>]+[\\"\'\s])(onmouseover|onmousedown|onmouseup|onmouseout|onmousemove|onclick|ondblclick|onload|xmlns)[^>]*>#iU', ">", $text);

            // remove javascript: and vbscript: protocol

            $text = preg_replace('#([a-z]*)=([\`\'\"]*)script:#iU', '$1=$2nojscript...', $text);
            $text = preg_replace('#([a-z]*)=([\`\'\"]*)javascript:#iU', '$1=$2nojavascript...', $text);
            $text = preg_replace('#([a-z]*)=([\'\"]*)vbscript:#iU', '$1=$2novbscript...', $text);

            //<span style="width: expression(alert('Ping!'));"></span> (only affects ie...)
            $text = preg_replace('#(<[^>]+)style=([\`\'\"]*).*expression\([^>]*>#iU', "$1>", $text);
            $text = preg_replace('#(<[^>]+)style=([\`\'\"]*).*behaviour\([^>]*>#iU', "$1>", $text);
            return $text;
        }

        // Formats the text of the story when displayed on screen.
        public static function format_story($text)
        {
            $text = trim($text);
            if (strpos($text, "<br>") === false && strpos($text, "<p>") === false && strpos($text, "<br />") === false) $text = self::nl2br2($text);
            if (_CHARSET != "ISO-8859-1" && _CHARSET != "US-ASCII") return stripslashes($text);
            return self::fix_bad_words($text);
        }

        public static function fix_bad_words($text)
        {
            $badwordchars = array(chr(212), chr(213), chr(210), chr(211), chr(209), chr(208), chr(201), chr(145), chr(146), chr(147), chr(148), chr(151), chr(150), chr(133));
            $fixedwordchars = array('&#8216;', '&#8217;', '&#8220;', '&#8221;', '&#8212;', '&#8211;', '&#8230;', '&#8216;', '&#8217;', '&#8220;', '&#8221;', '&#8212;', '&#8211;', '&#8230;');
            $text = str_replace($badwordchars, $fixedwordchars, stripslashes($text));
            return $text;
        }

        public function storytext($text,$imgfix=true){
            return  self::format_story($text);
        }

        public function raw_text($text){
            $text = stripslashes($text);
            return  $text;
        }

        public function img_data_src($text){
            return preg_replace('/\<img([^>]*)\ssrc=(\'|\")([^>]*)\2\s([^>]*)\/\>/', "<img$1 src='/src/img/blank.gif' data-src=$2$3$2 $4/>",$text);
        }

        public function slug($text){
            return preg_replace("/[\-]+/","-",preg_replace ("/[^\w]/","-",$text));
        }

        public function xmlentities ( $string )
        {
            return str_replace ( array ( '& ', '"', "'", '<', '>' ), array ( '&amp; ' , '&quot;', '&apos;' , '&lt;' , '&gt;' ), $string );
        }

        public function texttailfix($text){
            return preg_replace("/(.*);(\&[\#0-9]+)$/","$1;",$text);
        }
    }

    TextUtils::$INSTANCE = new TextUtils();

}


