<!DOCTYPE html>
<html>
<head lang="en">
	<title>
		IMAGE [{$site}:{$id}/{$cloud_name}/{$size}/{$public_name}]
	</title>
	<link href="/lib/swimauth/swimauth/src/style.css" rel="stylesheet">

</head>

<body class="main">
	<div class="box">
		<div class="container-1">
			<span class="icon"><i class="fa fa-search"></i></span>
			<input type="search" id="search" placeholder="Search..." value="{$search}" />
		</div>
	</div>
	<div class="tools">
		<span onclick="show_bbc()" target="_blank" href="#"><small class="fa">[BBC]</small></span>
		<span onclick="show_html()" target="_blank" href="#"><i class="fa fa-code"></i></span>
		<a id="original" target="_blank" href="#"><i class="fa fa-link"></i></a>
		<a id="source" target="_blank" href="#"><i class="fa fa-external-link"></i></a>
	</div>
	<div class="main_image_wrap rside300">
		<center>
			<img id="image"  classz="{if $size eq "mth"}fit-view{/if}"
			src= "/src/img/blank.gif"
			data-src="http://cloud.sexyfrenz.com/{$site}{$id}/{$cloud_name}/{$size}/{$public_name}.{$format}">
		</center>
		<div class="clear"></div>
	</div>
	<div class="image_info_wrap">
		<input id="addCeleb2Image"/>
		<script type="text/qkr-tmpl" id="image_info_template" >
			<% for(var i in image.sharedCeleb){ %>
				<div class="addedOption2Image" ><%= image.sharedCeleb[i].title %>
				<span class="fa fa-times remove_from_image" pointer data-celeb_id="<%= image.sharedCeleb[i].id %>" ></span></div>
			<% } %>
			<hr/>
			<% for(var i in image.sharedCollection){ %>
				<div class="addedOption2Image" ><%= image.sharedCollection[i].title %>
				<span class="fa fa-times remove_from_image" pointer data-collection_id="<%= image.sharedCollection[i].id %>" ></span></div>
			<% } %>
			<% for(var i in image.sharedAlbum){ %>
				<div class="addedOption2Image" ><%= image.sharedAlbum[i].title %>
				<span class="fa fa-times remove_from_image" pointer data-album_id="<%= image.sharedAlbum[i].id %>" ></span></div>
			<% } %>
			<hr/>OPTIONS<hr/>
			<% for(var i in options.CELEBS){ if(options.CELEBS[i]){ %>
				<span class="addOption2Image" pointer 
					data-celeb_id="<%= options.CELEBS[i].id %>" 
					title="<%= options.CELEBS[i].title %>" >
					<%= options.CELEBS[i].title %>
				</span>
			<% }} %>
			<% for(var i in options.COLLECTIONS){ if(options.COLLECTIONS[i]){ %>
				<span class="addOption2Image" pointer 
					data-collection_id="<%= options.COLLECTIONS[i].id %>" 
					title="<%= options.COLLECTIONS[i].title %>">
					<%= options.COLLECTIONS[i].title %>
				</span>
			<% }} %>
			<% for(var i in options.ALBUMS){ if(options.ALBUMS[i]){ %>
				<span class="addOption2Image" pointer 
					data-album_id="<%= options.ALBUMS[i].id %>" data-uid="<%= options.ALBUMS[i].uid %>" 
					title="<%= options.ALBUMS[i].title %>">
					<%= options.ALBUMS[i].title %>
				</span>
			<% }} %>
		</script>
		<div id="image_info" qkr-tmpl="image_info_template" url-image="image_info" url-options="image_options">
		</div>
		<hr/>
		
		<button id="saveImage2Story" class="fa fa-save" style="left:0px; width:30px;height:27px" value="Save" type="button" pointer></button>
		<input id="addImage2Story" style="min-width:100px; width:100px;"/>
		<input id="addImage2StoryNote" style="width:90%;" placeholder="Story Note"/>
		<script type="text/qkr-tmpl" id="image_story_info_template">
			<% for(var i in image.stories){ %>
				<div class="addedOption2Story" ><%= image.stories[i].title %>
				<span class="fa fa-times remove_from_story" pointer 
				data-sid="<%= image.stories[i].sid %>" 
				data-image_id="<%= image.info.id %>"
				></span></div>
			<% } %>
			<hr/>
			OPTIONS<hr/>
			<% for(var i in options.STORIES){ if(options.STORIES[i]){ %>
				<span class="addOption2Story" pointer 
					data-sid="<%= options.STORIES[i].id %>"
					title="<%= options.STORIES[i].title %>" data-title="<%= options.STORIES[i].title %>" >
					<%= options.STORIES[i].title %>
				</span>
			<% }} %>
		</script>
		<div id="image_story_info" qkr-tmpl="image_story_info_template" url-image="image_story_info" url-options="image_story_options">
		</div>
	</div>
	<link href="/lib/swimauth/swimauth/src/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="/lib/swimauth/swimauth/src/select2.css" rel="stylesheet"/>
	<script type="application/javascript" src="/src/external/components/webmodules-jquery/jquery.min.js"></script>
	<script type="application/javascript" src="/lib/swimauth/swimauth/src/swim.js"></script>
	<script type="application/javascript" src="/lib/swimauth/swimauth/src/autocomplete.js"></script>
	<script type="application/javascript" src="/lib/swimauth/swimauth/src/select2.min.js"></script>
	<script type="application/javascript" src="/src/external/components/quikrs-quikrjs/quikr.js"></script>
	<script type="application/javascript">
	var hashmap = {}
	var EROTIC_PAD = "http://www.eroticpad.com";
	function setHash(key,value){
		hashmap[key] = value;
		hashQuery = "?" + $.param(hashmap);
		window.location.hash = hashQuery;
		return hashQuery;
	}
	var $size = "{$size}",$site="{$site}",$id="{$id}" || "", $cloud_name="{$cloud_name}", $search = "{$search}",
	$public_name="{$public_name}",$format="{$format}";
	$CLOUD_CONTEXT = "{$CLOUD_CONTEXT}";
	var searchType = swim.isHashSearch($cloud_name) ? $cloud_name : "hash";
	var $image = $("#image");
	var version = (new Date()).getTime();

	function get_sexy_url (context, cloud_name,cloud_id,size, format){
		context = context || "temp";
		cloud_name = cloud_name || "hash";
		return "http://cloud.eroticpad.com/"+context+"/"+cloud_name+"/" + size + "/"
		+ cloud_id + "." + (format || 'png');
	};
	function get_sexy_image (o,_size){
		console.error(o,_size);
		var size = _size || "xs";
		if(o.sfid){
			return get_sexy_url("sf"+o.sfid,o.cloud_name,o.cloud_id,size,o.format);
		} else if(o.cxid){
			return get_sexy_url("cx"+o.cxid,o.cloud_name,o.cloud_id,size,o.format);
		} else if(o.cloud_id && o.cloud_name!="id"){
			return get_sexy_url($site,o.cloud_name,o.cloud_id,size,o.format);
		} else if(o.tseid){
			return get_sexy_url( "tse"+o.tseid ,o.cloud_name,o.cloud_id,size,o.format);
		} 
	}
	function loadImage (src) {
		console.error("src=",src)
		var img = new Image();
		$image[0].src = "/src/img/blank.gif";
		img.onload = function() {
			if (!! $image[0].parent)
				$image[0].parent.replaceChild(img, $image[0])
			else
				$image[0].src = src;
		}
		img.onerror = function() {
			$image[0].src = "/src/img/404.gif";
		}
		img.src = src;
	}

	function createImageSrc ($_size){
		return get_sexy_image({
			sfid : (($site == "sf") ? $id : undefined), 
			cxid : (($site == "cx") ? $id : undefined),
			tseid : (($site == "tse") ? $id : undefined),
			site : $site, cloud_name : $cloud_name, cloud_id : $public_name, format : $format
		},$_size || $size)
	}

	function reloadImage(){
		loadImage(createImageSrc());
	}

	function getImageInfoObject(){
		var full = isURL($search) ? $search : undefined;
		var id = (($site == "{$CLOUD_CONTEXT}" && $id) ? $id : undefined);
		var cloud_name = swim.isHashSearch($cloud_name) ? undefined : $cloud_name;
		var public_name = cloud_name ? $public_name : undefined;
		return swim.filter({
			full : full, id : id,
			cloud_name : cloud_name, public_name : public_name,
			cloud_id : public_name, format : $format
		});
	}

	$("#search").change(function(){
		$("#original,#source").attr("href","#");
		$id = undefined;
		$search = $("#search").val()
		var public_name = btoa($("#search").val());
		setHash("search",public_name)
		$public_name = public_name;
		$cloud_name = searchType;
		reloadImage();
		quikr.applyTmpl(document.getElementById('image_info'))
	});

	$("body").on("click","#image",function(){
		$("#image").toggleClass("fit-view");
		$size = ($size == "lg") ? "mth" : "lg";
		reloadImage();
	});
	var OPTIONS = { COLLECTIONS : {}, CELEBS : {}, ALBUMS : {} };
	function refeshOptions(){
		var optionString = localStorage.getItem("options");
		try {
			OPTIONS = optionString ? JSON.parse(optionString) : OPTIONS;
		} catch(e){
			OPTIONS = OPTIONS;
		}
		OPTIONS.ALBUMS  = OPTIONS.ALBUMS || {}; OPTIONS.COLLECTIONS = OPTIONS.COLLECTIONS|| {}; OPTIONS.CELEBS = OPTIONS.CELEBS|| {};
		OPTIONS.STORIES = OPTIONS.STORIES  || {};
		return OPTIONS;
	}
	function getOptions(){
		return refeshOptions();
	}
	function saveOptions(){
		localStorage.setItem("options",JSON.stringify(OPTIONS));
	}
	refeshOptions();
	var progress = {};
	console.error("image_info",$search);
	var func_image_info = function(){
		console.error("====",$search);
		var fulld = isURL($search) ? btoa($search) : undefined;
		var urlkey = "/api/picaso/image/details?" + $.param($.extend({
			prevent : true, populate : true, _ : version
		},getImageInfoObject()));
 		progress[urlkey] = progress[urlkey] || $.getJSON(urlkey).done(function(resp){
 			$id = resp.id;
 			$cloud_name = resp.cloud_name || $cloud_name ;
 			$public_name = resp.public_name || $public_name;
 			if($id){
				$site = "{$CLOUD_CONTEXT}";
 			}
 			delete progress[urlkey];
 		})
		return progress[urlkey];
	};
	quikr.url("image_info", function(){
		return func_image_info();
	}).url("image_options",function(){
		console.error("READON",OPTIONS);
		return getOptions();
	}).url("image_story_info", function(){
		return func_image_info().then(function(resp){
			console.error("image_story_info",resp);
			var urlkey = EROTIC_PAD + "/json/image_info?" + $.param({
				prevent : true, populate : true, _ : version,
				sfid : resp.id
			});
	 		progress[urlkey] = progress[urlkey] || $.post(urlkey).done(function(resp){
	 			delete progress[urlkey];
	 		});
			return progress[urlkey];
		});
	}).url("image_story_options",function(){
		console.error("READON",OPTIONS);
		return getOptions();
	});

	{literal}
	$(document).ready(function(){
		var hash = window.location.hash.replace("#","") || "";
		if(hash && hash.indexOf("?")!=0){
			hash = setHash("search",hash)
		} 
		if(hash && hash.indexOf("?")==0){
			decodeURIComponent(hash.replace("?","")).split("&").map(function(param){
				var params = param.split("=");
				hashmap[params.shift()] = params.join("=");
			});
		}
		setHash("_","_");
		if(hashmap["search"]){
			var search = atob(hashmap["search"]);
			$("#search").val(search);
			$search = $("#search").val()
			$('body').addClass('results');
			$("#search").change();
			refeshOptions();
		} else {
			loadImage($image.attr("data-src"));
		}

		$("body").on("click",".remove_from_image", function(e){
			console.error($site,$CLOUD_CONTEXT,$id);
			$.post("/api/picaso/collection/image/remove", swim.filter({
				image_id : (($site == $CLOUD_CONTEXT && $id) ? $id : undefined),
				celeb_id : e.target.dataset.celeb_id,
				collection_id : e.target.dataset.collection_id,
				story_id : e.target.dataset.story_id,
				scene_id : e.target.dataset.scene_id,
				album_id : e.target.dataset.album_id
			})).done(function(){
				version++;
				quikr.applyTmpl(document.getElementById('image_info'));
			});
		});

		function addChoice(choice){
			var _options = getOptions();
			var title = choice.attr('title');
			var image_id =  choice.data("image_id");
			var collection_id =  choice.data("collection_id");
			var celeb_id =  choice.data("celeb_id");
			var story_id =  choice.data("story_id");
			var scene_id =  choice.data("scene_id");
			var album_id =  choice.data("album_id");
			var uid =  choice.data("uid");
			if(collection_id){
				_options.COLLECTIONS[collection_id] = { id : collection_id, title :title};
			} else if(celeb_id){
				_options.CELEBS[celeb_id] = { id : celeb_id, title :title};
			} else if(album_id){
				_options.ALBUMS[uid+"_"+album_id] = { id : album_id, title :title, uid : uid};
			}
			saveOptions();
			//localStorage.setItem("options",JSON.stringify(OPTIONS));
       		return $.post("/api/picaso/image/details",$.extend({
				celeb_title : title, celeb_id : celeb_id, collection_id : collection_id,
				story_id : story_id,scene_id : scene_id, album_id : album_id,
				prevent : 0, populate : 1, uid : uid
			},getImageInfoObject())).done(function(resp){
				version++;
				quikr.applyTmpl(document.getElementById('image_info'));
			});
		};

		$("body").on("click",".addOption2Image" ,function(e){
			addChoice($(e.target));
		});

		$("body").on("focus","#addCeleb2Image" ,function(e){
			var $elem = $(this);
			if(!$elem.attr("search-added")){
				$elem.yourlabsAutocomplete({
		            url: '/api/search_people.html?imdb=0&user=0&collections=1&blogger=1',
		            choiceSelector: 'a',
		        }).input.bind('selectChoice', function(e, choice, autocomplete) {
		           $("#addCeleb2Image").val(choice.attr('title'));
					addChoice(choice).done(function(){
						$("#addCeleb2Image").val("");
					})
		        });
		        $elem.attr("search-added","search-added");
			}
		});
		$("#addImage2Story").select2({
	        query: function (e, b) {
	            console.error("query", arguments);
	            $.get(EROTIC_PAD+ "/json/story_list_search.json", {
	                search: e.term,
	                limit: 50
	            }).done(function (resp) {
	                e.callback((function(resp, term) {
	        			return {
				            results: resp.map(function (item) {
				                return {
				                    id: item.sid,
				                    text: item.title + " (" + item.penname + ")"
				                }
				            })
				        };
			    	})(resp, e.term));
	            });
	        }
	    });
	    function add2Story(sid, title){
	    	var _options = getOptions();
	    	var img = getImageInfoObject();
	    	img.sfid = img.id;
	    	var sid = sid;
	    	var title = title;
	    	_options.STORIES[sid] = { id : sid, title :title};
	        jQuery.post(EROTIC_PAD + "/json/image_details", swim.filter({
		            full: img.full,
		            sid: sid,
		            scene_note: $("#addImage2StoryNote").val()
		        }
		    )).done(function (resp) {
				version++;
				quikr.applyTmpl(document.getElementById('image_story_info'));
				saveOptions();
				//localStorage.setItem("options",JSON.stringify(OPTIONS));
		    });
	    }
	    $("#saveImage2Story").click(function () {
			add2Story($("#addImage2Story").val(),$("#addImage2Story").select2('data').text);
	    });
	    $("body").on("click",".addOption2Story" ,function(e){
			add2Story(e.target.dataset.sid,e.target.dataset.title);
		});
		$("body").on("click",".addedOption2Story" ,function(e){
			jQuery.post(EROTIC_PAD + "/json/story_add_image", swim.filter({
		            image_id: e.target.dataset.image_id,
		            sid: e.target.dataset.sid,
		            update: 2
		        }
		    )).done(function (resp) {
				version++;
				quikr.applyTmpl(document.getElementById('image_story_info'));
		    });
		});
		$.ajaxSetup({
		    type: "POST",
		    data: {},
		    dataType: 'json',
		    xhrFields: {
		       withCredentials: true
		    },
		    crossDomain: true
		});
		quikr.init();
	});

		function isURL(str) {
		  var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
		  '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|'+ // domain name
		  '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
		  '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
		  '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
		  '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
		  return pattern.test(str);
		}
	{/literal}

	function show_bbc(){
		window.prompt("Copy",[
			'[URL="',createImageSrc("html"),'"]',
			'[IMG]',createImageSrc("sm"),'[/IMG]',
			'[/URL]'
			].join(''));
	}
	function show_html(){
			window.prompt("Copy",[
			'<a href="',createImageSrc("html"),'">',
			'<img src="',createImageSrc("sm"),'"/>',
			'</a>'
			].join(''));
	}
	</script>

</body>
</html>