<?php
/**
 * Created by PhpStorm.
 * User: iMCDb
 * Date: 10/16/16
 * Time: 7:02 PM
 */

namespace app\service;

class SwimAuth
{
    public static $OTP_SERVER = "http://sso.localhost.com/"; //"http://sso.parichya.com/";
    public static $OTP_BROKER_ID = ""; //"BROKER_PUBLIC_KEY";
    public static $OTP_BROKER_SECRET = "******"; //"http://sso.parichya.com/";
    public static $OTP_SESSION_DATA = null;
    public static $RETURN_URL;
    public static $OTP_AUTH_INFO = "user_data";
    public static $OTP_AUTH_TOKEN = "session_token";
    public static $OTP_CALLBACK_URL_KEY = "broker_url";
    public static $OTP_DEST_URL_KEY = "dest_url";
    public static $OTP_LOGOUT_URL_KEY = "logout_url";
    public static $CLOUD_CONTEXT = "no";
    public static $CLOUD_SERVER = "http://cloud.eroticpad.com";

    public static function rx_setup()
    {
        if (class_exists('\Config')) {
            $options = \Config::get("SWIMAUTH_CONFIG");
            if (!is_null($options) && !empty($options)) {
                return self::setUp($options);
            }
        } else {
            echo "No Exists";
        }
    }

    public static function use_smarty_tags()
    {
        return Smarty::addPluginsDir("../plugins");
    }

    public static function setUp($options = array())
    {
        self::$CLOUD_CONTEXT = isset($options["CLOUD_CONTEXT"]) ? $options["CLOUD_CONTEXT"] : self::$CLOUD_CONTEXT;
        self::$OTP_SERVER = isset($options["SERVER"]) ? $options["SERVER"] : self::$OTP_SERVER;
        self::$OTP_BROKER_ID = isset($options["BROKER_ID"]) ? $options["BROKER_ID"] : self::$OTP_SERVER;
        self::$OTP_BROKER_SECRET = isset($options["BROKER_SECRET"]) ? $options["BROKER_SECRET"] : self::$OTP_SERVER;
    }

    public static function getSessionToken()
    {
        if (isset($_SESSION["otp-" . self::$OTP_AUTH_TOKEN])) {
            return $_SESSION["otp-" . self::$OTP_AUTH_TOKEN];
        }
        return "";
    }
    public static function VAL($key,$value=null)
    {
        if(!is_null($value)){
            $_SESSION["otp-" . $key] = $value;
        } 
        if (isset($_SESSION["otp-" . $key])) {
            return $_SESSION["otp-" . $key];
        }
        return "";
    }


    public static function authenticate($options = array(), $session_token = null)
    {
        self::$OTP_SESSION_DATA = isset($_SESSION[self::$OTP_AUTH_INFO]) ? $_SESSION[self::$OTP_AUTH_INFO] : null;

        if (isset($_REQUEST[self::$OTP_AUTH_TOKEN])) {
            $session_token = $_REQUEST[self::$OTP_AUTH_TOKEN];
            self::$OTP_SESSION_DATA = null;
        }
        if (self::$OTP_SESSION_DATA == null) {
            if (!isset($_REQUEST[self::$OTP_AUTH_TOKEN]) && $session_token == null) {
                $otpAuthOptions = array(
                    "broker_id" => self::$OTP_BROKER_ID
                );
                $CALLBACK_URL = "http://$_SERVER[HTTP_HOST]/swim/auth/callback";
                $otpAuthOptions[self::$OTP_CALLBACK_URL_KEY] = $CALLBACK_URL;
                $RETURN_URL = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                if (isset($options[self::$OTP_DEST_URL_KEY]) && !empty($options[self::$OTP_DEST_URL_KEY])) {
                     $RETURN_URL = $options[self::$OTP_DEST_URL_KEY];
                } 
                self::VAL(self::$OTP_DEST_URL_KEY,$RETURN_URL);

                $redirectUrl = (self::$OTP_SERVER . "/login?broker_id=" . $otpAuthOptions["broker_id"] .
                    "&" . self::$OTP_CALLBACK_URL_KEY . "d=" . base64_encode($otpAuthOptions[self::$OTP_CALLBACK_URL_KEY]));
                header("Location: " . $redirectUrl);
                //header("Location: " . self::$OTP_SERVER . "/login?" . http_build_query($otpAuthOptions));
                die();
            } else {
                //print_line("sending...post".self::$OTP_SERVER . "/getdata");
                self::$OTP_SESSION_DATA = json_decode(self::api("POST", self::$OTP_SERVER . "/getdata", array(
                    "broker_id" => self::$OTP_BROKER_ID,
                    "broker_secret" => self::$OTP_BROKER_SECRET,
                    "session_token" => $session_token
                )));
                self::VAL(self::$OTP_AUTH_TOKEN,$session_token);
                $_SESSION[self::$OTP_AUTH_INFO] = serialize(self::$OTP_SESSION_DATA);
                $_SESSION["swim-auth-done"] = TRUE;
            }
        } else {
            self::$OTP_SESSION_DATA = unserialize($_SESSION[self::$OTP_AUTH_INFO]);
        }
        return self::$OTP_SESSION_DATA;
    }

    public static function logout($redirectUrl=null,$options = array())
    {
        if(empty($redirectUrl)){
            $redirectUrl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        }
        self::VAL(self::$OTP_LOGOUT_URL_KEY,$redirectUrl);
        if (isset($_SESSION["swim-auth-done"]) && $_SESSION["swim-auth-done"] == TRUE) {
            unset($_SESSION["swim-auth-done"]);
            unset($_SESSION[self::$OTP_AUTH_INFO]);
            header("Location: " . self::$OTP_SERVER . "/logout?" . http_build_query(array(
                    "broker_id" => self::$OTP_BROKER_ID,
                    "broker_urld" => base64_encode("http://$_SERVER[HTTP_HOST]/swim/auth/logout"),
                    "command" => "logout",
                    "configd" => base64_encode(json_encode($options))
                )));
            exit();
        } else {
            header("Location: /swim/auth/logout?token=".microtime());
            exit();
        }
    }

    public static function authUserName($options)
    {
        self::$OTP_SESSION_DATA = json_decode(self::api("POST", self::$OTP_SERVER . "/getbyauthdata", array(
            "username" => $options["username"],
            "password" => $options["password"]
        )));
        $_SESSION[self::$OTP_AUTH_INFO] = serialize(self::$OTP_SESSION_DATA);
        return self::$OTP_SESSION_DATA;
    }

    public static function api($method, $url, $data = false)
    {
        $curl = curl_init();

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // Optional Authentication:
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        //curl_setopt($curl, CURLOPT_USERPWD, "username:password");

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }

}

\app\service\SwimAuth::rx_setup();