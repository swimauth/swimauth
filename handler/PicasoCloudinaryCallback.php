<?php

namespace app\handler;

use app\service\R;
/**
 *
 * @Handler(cloudinary)
 *
 */
class PicasoCloudinaryCallback extends CloudinaryCallback
{
    public $user_id = null;
    public $cloud_name = null;
    public $collection_id = null;
    public $album_id = null;
    public $chapter_id = null;
    public $full = null;
    public $title = null;

    public $image = null;

    public function image_upload_callback($imageData)
    {
        $image  = $this->image;
        $user = null;

        if(empty($image) && !empty($imageData)){
            if (!is_null($this->cloud_name) && !empty($this->cloud_name)) {
                $image = R::findOne("image", "public_name=? AND cloud_name=?",
                    array($imageData["public_id"],$this->cloud_name));
            } else {
                $image = R::findOne("image", "public_name=?", array($imageData["public_id"]));
            }
            if (is_null($image)) {
                $image = R::dispense("image");
                $image->public_name = isset($imageData["public_id"]) ? $imageData["public_id"] : null;
                $image->created_at =  isset($imageData["created_at"]) ? $imageData["created_at"] : null;
                $image->format = isset($imageData["format"]) ? $imageData["format"] : null;
                $image->height = isset($imageData["height"]) ? $imageData["height"] : null;
                $image->width = isset($imageData["width"]) ? $imageData["width"] : null;
                //$image->url = isset($imageData["url"]) ? $imageData["url"] : null;
                $image->original_filename =isset($imageData["original_filename"]) ? $imageData["original_filename"] : null;
                $image->version = isset($imageData["version"]) ? $imageData["version"] : null;
                $image->cloud_name = $this->cloud_name;
                $image->inorder = microtime();
                $image->likes = 0;
                $image->checktime = 0;
                $image->tumblr = null;
                $image->twitter = null;
            }
            if (isset($imageData["path"])) {
                $image->path = $imageData["path"];
            }
        }

        if (!is_null($this->cloud_name) && empty($image->cloud_name)) {
            $image->cloud_name = $this->cloud_name;
        }
        if (!is_null($this->full)) {
            $image->full = $this->full;
        }
        if (!is_null($this->title)) {
            $image->title = $this->title;
        }

        if(empty($this->user_id) || $this->user_id < 0){
            $this->user_id = 1;
        } else {
            $user = R::findOne("user","id=?",array($this->user_id));
        }

        if (!empty($this->user_id)) {
            $image->user_id = $this->user_id;
        }

        //return $image;
        R::store($image);

        $likes = R::dispense("likes");
        $likes->image_id = $image->id;
        $likes->user_id = $image->user_id;
        $likes->liked = 0;
        R::store($likes);

        $matchs;

        if(!empty($this->collection_id) && preg_match("/^_([0-9]+)$/", $this->collection_id , $matchs) == 1){
            $this->collection_id = null;
            $this->album_id = $matchs[1];
        } else if(!empty($this->collection_id) && preg_match("/c([0-9]+)/", $this->collection_id , $matchs) == 1){
            $this->collection_id = null;
            $this->celeb_id = $matchs[1];
        }
        //echo "SAB_HONE_SE_PEHLE";

        $collection = null;
        if ($this->collection_id == "_COLLECTION_" && !empty($user)) {
            $collection = R::findOne("collection", "( description=? OR def=? ) AND uid=?", array("Wall Collection",1,$this->user_id));
            if(empty($album)){
                 $album = R::dispense("collection");
            }
            $collection->title = ($user->penname . "'s Wall");
            $collection->description = "Wall Collection";
            $collection->private = 0;
            $collection->def = 1;
            $collection->user_id = $this->user_id;
            $collection->uid = $this->user_id;
            $collection->updated = microtime(true);
            $this->collection_id = R::store($collection);
        } else if(!empty($this->collection_id)){
            $collection = R::load("collection", $this->collection_id);
        }

        //echo "SAB_HONE_SE_PEHLE";
        $album = null;
        if ($this->album_id == "_TIMELINE_"  && !empty($user)) {
            $album = R::findOne("album", "( description=? OR def=? ) AND user_id=?", array("My Own Album",1 ,$this->user_id));
            if(empty($album)){
                 $album = R::dispense("album");
            }
            $album->title = ($user->penname."'s Pics");
            $album->description = "My Own Album";
            $album->private = 0;
            $album->def = 1;
            $album->user_id = $this->user_id;
            $album->updated = microtime(true);
            $album->public_name = null;
            $album->cloud_name = null;
            $this->album_id = R::store($album);
        } else if(!empty($this->album_id)){
            $album = R::load("album",$this->album_id);
        }

        $chapter = null;
        if(!empty($this->chapter_id)){
            $chapter = R::load("chapter",$this->chapter_id);
        }

        $celeb = null;
        if(!empty($this->celeb_id)){
            $celeb = R::load("celeb", $this->celeb_id);
        }

       if(!is_null($image)){
            if (!is_null($collection)) {
                $imagecollection = R::findOne("imagecollection","image_id=? AND collection_id = ?", array(
                    $image->id, $collection->id
                ));
                if(empty($imagecollection)){
                   $imagecollection = R::dispense("imagecollection");
                    $imagecollection->image = $image;
                    $imagecollection->collection = $collection;
                    $imagecollection->added = microtime(true);
                    $collection->updated = microtime(true);
                    $collection->count++;
                    R::store($imagecollection);
                }
            }
            if (!is_null($album)) {
                if(empty($image->album_id)){
                    $image->album = $album;
                    R::store($image);
                }
                $imagealbum = R::findOne("imagealbum","image_id=? AND album_id = ?", array(
                    $image->id, $album->id
                ));

                if(empty($imagealbum)){
                    $imagealbum = R::dispense("imagealbum");
                    $imagealbum->image = $image;
                    $imagealbum->album = $album;
                    $imagealbum->added = microtime(true);
                    R::store($imagealbum);
                }

                 if (empty($album->public_name) || empty($album->image_id)) {
                    $album->cloud_name = $image->cloud_name;
                    $album->public_name = $image->public_name;
                    $album->format = $image->format;
                    $album->image = $image;
                    R::store($album);
                }

            }

            if (!is_null($celeb)) {
                $celeb->sharedImageList[] = $image;
                R::store($celeb);
            }

            if (!is_null($chapter)) {
                $page = R::dispense("page");
                $page->image = $image;
                $page->chapter = $chapter;
                $page->inorder = 0;
                $page->added = microtime(true);
                R::store($page);
            }
        }

        return $image;
    }
}