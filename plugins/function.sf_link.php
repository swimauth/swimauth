<?php
/**
 * Smarty plugin
 *
 * @package Smarty
 * @subpackage PluginsFunction
 */

/**
 * Smarty {seo} function plugin
 *
 * Type:     function<br>
 * Name:     seo<br>
 * Date:     Dic 05, 2012
 * Purpose:  seo url friendly.<br>
 * Params:
 * <pre>
 * - string - (required) - Title to friendly URL conversion
 * - divider - (required) - return good words separated by dashes
 * </pre>
 * Examples:
 * <pre>
 * {seo string="Lorem Ipsum"}
 * {seo string="Lorem Ipsum" divider="_"}
 * </pre>
 *
 * @version 1.0
 * @author Concetto Vecchio <info@cvsolutions.it>
 * @param array $params parameters
 * @param Smarty_Internal_Template $template template object
 * @return string
 */

function smarty_function_sf_link($params, $template)
{
	$link = empty($params["l"]) ? "home" : $params["l"];
	$page = empty($params["page"]) ? 1 : $params["page"];
	$title = \app\utils\TextUtils::$INSTANCE->slug(empty($params["title"]) ? "" : $params["title"]);
	$id = empty($params["id"]) ? "" : $params["id"];
	$image = empty($params["image"]) ? 0 : $params["image"];
	$album = empty($params["album"]) ? 0 : $params["album"];
	$hash = empty($params["hash"]) ? "" : $params["hash"];
	$collection = empty($params["collection"]) ? "0" : $params["collection"];
	$uid = empty($params["uid"]) ? 0 : $params["uid"];
	$ext = empty($params["ext"]) ? "html" : $params["ext"];
	$celeb = empty($params["celeb"]) ? "0" : $params["celeb"];
	$version = empty($params["_"]) ? ( isset($_GET["_"]) ? $_GET["_"] : 0 ): $params["_"];

	switch ($link) {
		case 'trending':
			return sprintf("/trending/page_%s.%s",$page,$ext).("?_=".$version);
		break;
		case 'celeb':
			return sprintf("/celebs/%s/%s/%s.html",$celeb,$page,$title).("?_=".$version);
		break;
		case 'collections':
		case 'collection':
			if(empty($id)){
				return sprintf("/collection/0/%s/latest.%s",$page,$ext).("?_=".$version);
			}
			return sprintf("/collection/%s/%s/%s.%s",$id,$page,$title,$ext).("?_=".$version);
		break;
		case 'mycollections':
			return sprintf("/u/collection/").("?_=".$version);
		break;
		case 'image':
			if(!empty($celeb)){
				return sprintf("/celeb_%s/image_%s.html",$celeb,$image).("?_=".$version);
			} else if(!empty($album)){
				return sprintf("/album_%s/image_%s.%s",$album,$image,$ext).("?_=".$version);
			} else {
				return sprintf("/image/%s_%s.%s",$collection,$image,$ext).("?_=".$version);
			}
		break;
		case 'myalbums':
			return sprintf("/albums",$id,$page,$title).("?_=".$version);
		break;
		case 'albums':
			return sprintf("/user_%s/albums.html",$uid).("?_=".$version);
		break;
		case 'album':
			return sprintf("/album_%s/image_%s.html",$id,$image).("?_=".$version);
		break;
		default:
		return $link;
		break;
	};
}
