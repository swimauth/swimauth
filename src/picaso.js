/**
 * Created by iAdmin on 5/9/17.
 */

(function(){
  var newImages = []; var config = [];
    var LAST_URL;
    var START_URL = null;
    var LAST_RESPONSE = null;
   var _newImages = localStorage.getItem("newImages");
   if(_newImages){
        try {
            newImages = JSON.parse(_newImages);
        } catch(e){
            newImages = [];
        }
   }
    quikr.cache_time = 2000;
    var RELOAD_IMAGES = false;
    function uniq(a) {
        var seen = {};
        return a.filter(function(item) {
            return seen.hasOwnProperty(item.src) ? false : (seen[item.src] = true);
        });
    }
    var refreshData = debounce(function(){
        console.warn("refreshData",RELOAD_IMAGES)
        if(RELOAD_IMAGES){
            newImages = uniq(newImages);
            quikr.applyTmpl(document.getElementById("images_bucket"));
            RELOAD_IMAGES = false;
        }
        localStorage.setItem("newImages",JSON.stringify(newImages));
        document.getElementById("total_images").innerHTML = "&nbsp;( "+newImages.length + " )&nbsp;";
    },quikr.cache_time), _on_action_taken_ = function(elem){
        var $box = $(elem).closest(".thumb_image_box");
        $box.parent().append($box);
     };

    var $details = null;
    quikr.url("/api/me/detail", function(){
         $details = $details || $.post("/api/me/detail").done(function(resp){
            //console.error("resp",resp)
            if(resp.uid>0 && resp.valid && window.ga){
                ga('set', 'userId', resp.penname || resp.uid); // Set the user ID using signed-in user_id.
            }
         });
         return $details;
    }).url("images", function(){
        console.error("newImages",newImages)
        return newImages || [];
    }).url("configs",function(){
        console.error("config",config)
        return config;
    }).action("image_remove",function(data){
        $(this).addClass("text-muted");
        newImages = newImages.filter(function(item){
            if(item.src == data.src){
                item.removed = true;
                return false;
            } 
            return true;
        });
        RELOAD_IMAGES = true;
        _on_action_taken_(this);
        refreshData();
    }).action("image_done",function(data){
        $(this).addClass("text-muted");
        _on_action_taken_(this);

        newImages = newImages.filter(function(item){
            if((item.src == data.src)){
                console.error("DONE MARKED")
                item.done = true;
            }
            return true;
        });

        uploadImage({
            url : data.url,
            collection_id : $('#collection_id').val(),
            chapter_id : $('#chapter_id').val(),
            album_id : $('#album_id').val(),
            celeb_id : $('#celeb_id').val(),
            uid : data.uid
        }).done(function(){
            newImages = newImages.filter(function(item){
                if(item.src == data.src){
                    item.done = true;
                    return false;
                } 
                return true;
            });
            RELOAD_IMAGES = true;
            refreshData();
        });
    });

     
    var uploadImage = function(data, imageUrl,collection_id,uid){
        if(!data.url){
            return;
        }
        return $.post("/api/picaso/image/details", JSON.parse((JSON.stringify({
                full: data.url,
                uid: data.uid,
                collection_id: data.collection_id, album_id: data.album_id,
                chapter_id: data.chapter_id, celeb_id: data.celeb_id,
            })).replace(/http/g, 'ht@#tp')
                .replace(/cock/g, 'co@#ck').replace(/viagra/g, 'vi@#agra')
                .replace(/pussy/g, 'pu@#ssy').replace(/suck/g, 'su@#ck')
                .replace(/fuck/g, 'fu@#ck').replace(/dick/g, 'di@#ck')
        ));
    };

    $(document).ready(function(){
        function cleanurl(url){
           return  url.replace(/http:\/\/cloud7\.sexyfrenz\.com(:[0-9]*)?\/proxy\.php\?/g,'');
        }
        $.scrapGet = function(url,b,c,d){
            if(sameDomain(url,window.location.href)){
                return $.get(url,b,c,d);
            }
            return $.Deferred(function($dff){
                url = cleanurl(url);
                var http = (window.location.protocol === 'http:' ? 'http:' : 'https:');
                $.get(http + '//cors-anywhere.herokuapp.com/' + url,b,c,d).then(function(resp){
                    $dff.resolve(resp);
                }).fail(function(){
                    $.get('http://cloud7.sexyfrenz.com/proxy.php?' + url,b,c,d).then(function(resp){
                        $dff.resolve(resp);
                    }).fail(function(){
                        $dff.reject(resp);
                    });
                });

            }).promise();
        } 
        // $.ajaxPrefilter( function (options) {
        //     if (options.crossDomain && jQuery.support.cors 
        //         && (options.url.indexOf("literotica")>-1 || options.url.indexOf("xossip")>-1)) {
        //         var http = (window.location.protocol === 'http:' ? 'http:' : 'https:');
        //         options.url = http + '//cors-anywhere.herokuapp.com/' + options.url;
        //         //options.url = "http://cors.corsproxy.io/url=" + options.url;
        //     }
        // });

        function sameDomain(url1, url2){
           return (url1||"").replace(/https?:\/\//,'').split("/")[0] == (url2||"").replace(/https?:\/\//,'').split("/")[0]
        }

        function Rule(options){
            for(var i in options){
                this[i] = options[i];
            }
            this.urls = [];
            this._urls = {};
            this.index = Rule.index++;
            var jsonstring = localStorage.getItem("rule"+this.index);
            if(jsonstring){
                try{
                    jsonstring = JSON.parse(jsonstring);
                    this.urls = jsonstring.urls || [];
                    this._urls = jsonstring._urls || {};
                } catch(e){ console.error(e)}
            }
        }
        Rule.index = 0; Rule.paused = true; Rule.url = null,Rule.timer = null;;
        Rule.prototype.push = function(url) {
            if(url && url.indexOf("#")!=0 && url!="/"
                && !this._urls[url] && this.urls.indexOf(url)<0){
                this.urls.push(url);
            }
        };
        Rule.prototype.pop = function(url) {
            var url = this.urls.shift();
            this._urls[url] = true;
            localStorage.setItem("rule"+this.index, JSON.stringify({
                urls : this.urls, _urls : this._urls
            }));
            return url;
        };
        var getUrlParam = function(url,param){
           var pages = url.split(param+"=");
            if(pages[1]){
                var _page = pages[1].split("&");
                return _page[0];
            };
        };
        var initConfig = function(){
            //config = [];
            config.push(new Rule({
                name : "XOSSIP",
                url : function(url){
                    return url.indexOf("xossip.com")>0
                },
                img : function(src){
                    return src && src.indexOf("http")==0 && src.indexOf("xossip.com")==-1
                }, 
                next : function(url){
                    var pages = url.split("page=");
                    if(pages[1]){
                        var _page = pages[1].split("&");
                        return pages[0] + "page=" + (_page[0]-0+1) + "&";

                    }
                    return url + "&page=2";
                }
            }));
            config.push(new Rule({
                name : "TUMBLR",
                url : function(url){
                    return  url.indexOf("tumblr.com")>0
                },
                img : function(src,elem){
                    if( 
                        (src.indexOf(".tumblr.com/avatar_") > 0) 
                        || (src.indexOf("assets.tumblr.com") > 0)
                        || (src.indexOf("px.srvcs.tumblr.com")> 0)
                        ){
                        return false;
                    }
                    return src.indexOf("http")==0;
                }, 
                next : function(url,$html){
                    var self = this;
                    if(url.indexOf("/archive")>=0){
                        var before_time = getUrlParam(url,"before_time");
                        if(!before_time){
                            self.push(url + "?before_time="+Math.round(new Date().getTime()/1000) );
                        } else if(!isNaN(before_time)){
                            self.push(
                                url.split(/before_time=[0-9]*/).join("before_time="+ (before_time-3600*24*30)) 
                            );
                        }
                    }
                    if($html){
                        $html.find("a").each(function(i,elem){
                            if(elem.href 
                                && (/https?:\/(.*)\/post\/[\d]+(\/.*)?$/).test(elem.href)
                                && sameDomain(url,elem.href)
                                ){
                                self.push(elem.href);
                            }
                        });
                    }
                    return this.pop();
                }
            }));
            config.push(new Rule({
                name : "MYTWITTER",
                test : 9,
                url : function(url){
                    return  url.indexOf("/api/picaso/")>0
                },
                img : function(src,elem){
                    return src.indexOf("http")==0;
                }, 
                next : function(url,$html){
                    var self = this;
                    console.error("NEXT")
                    if($html){
                        $html.find(".navlink").each(function(i,elem){
                            if(elem.href 
                                ){
                                var href = elem.href;
                                self.push(href);
                            }
                        });
                    }
                    return this.pop();
                }
            }));
            config.push(new Rule({
                name : "NONE",
                url : function(url){
                    return true;
                },
                img : function(src,elem){
                    return src.indexOf("http")==0;
                }, 
                next : function(url){
                    return this.pop();
                }
            }));
        }
        initConfig();

        var fetchMore = function  (url,rule){
            console.error("fetch call=",url);
            clearTimeout(Rule.timer);
            if(Rule.paused){
                Rule.timer = setTimeout(function () {
                    fetchMore(url,rule)
                },1500);
                return;
            }
            if(!url) return;
            $('#current_url').text(url);
            var config_id = $('#configs_id').val();
            return $.scrapGet(url).done(function(resp){
                var c = rule || config.filter(function(item){
                    return (config_id ==item.name) || item.url(url);
                })[0];
                if(c){
                    var domain = url.match(/(https?:\/\/[^\/]*)\/(.*)$/)[1];
                    var thisResponse = cleanurl(resp.replace(/<(style|script|head|link)/g,"<dead")
                    .replace(/(style|script|head|link)>/g,"dead>"));
                    //console.error(thisResponse);
                    thisResponse=thisResponse.replace(
                         /\<img([^>]*)\ssrc=('|")([^>]*)\2\s*([^>]*)\/?\>/gi,
                        "<img$1 data-src=$2$3$2 $4>");
                    var $respHTML = null;
                    if(thisResponse == LAST_RESPONSE){
                        console.warn("Same Response");
                        $respHTML = $("<div>").html(thisResponse);
                    } else {
                       LAST_RESPONSE = thisResponse;
                        $respHTML = $("<div>").html(thisResponse);
                        $respHTML.find("img").each(function(i,elem){
                            var src= $(elem).attr("data-src");
                            if(src){
                                if(src.indexOf("/") == 0){
                                    src = domain + src;
                                }
                                if(c.img(src,elem)){
                                    src = cleanurl(src);
                                    newImages.push({
                                        src : btoa(src),
                                        url : src
                                    })
                                }
                            }

                        });
                        refreshData();
                        localStorage.setItem("lastUrl",url);
                        console.log("Image Saved");
                    }
                    fetchMore(c.next(url,$respHTML));
                }
            });
        }


        $("body").on("click","#upload_link",function (e,target) {
            uploadImage({
                url : window.prompt("Enter Url"),
                collection_id : this.dataset.collection_id,
                uid : this.dataset.uid,
                chapter_id : this.dataset.chapter_id,
                album_id : this.dataset.album_id,
                celeb_id : this.dataset.celeb_id,
            }).done(function (resp) {
                //document.location.reload();
            });
        });

        quikr.init();
        quikr.loadLazyImages(true,{
            blank : "/src/img/blank.gif", 404 : "/src/img/404.gif"
        });

        var lastUrl = localStorage.getItem("lastUrl");
        if(lastUrl){
            $('[name="url"]').val(lastUrl);
        }
        $("body").on("change", '[name="url"]',function(e){
            if(!e.target.value){
                var keysToRemove = ["lastUrl","newImages"];
                clearTimeout(Rule.timer);
                newImages = [];
                for(var key in localStorage){
                    if(key.indexOf("rule")==0 || keysToRemove.indexOf(key) >=0 ){
                        localStorage.removeItem(key);
                    }
                }
                RELOAD_IMAGES = true;
                refreshData();
                //initConfig();
            }
            var $d = fetchMore(e.target.value);
            $d && $d.done(function(){
                quikr.applyTmpl(document.getElementById("images_bucket"));
            });
        });

        $("body").on("change", '[name="started"]',function(e){
            Rule.paused = !e.target.checked;
            RELOAD_IMAGES = true;
            refreshData();
        });

        $('[name="url"]').change();

        if($().sortable){
            $(".sortable-thumbs").sortable();
        }
        $(".ordera2z").click(function(){
            var list = [];
            var $parent = $(".sortable-thumbs");
            $parent .children().each(function(i,elem){
                list.push(elem);
            });
            list = list.sort(function(a, b){
                if(a.dataset.title > b.dataset.title){
                    return 1;
                } else if(a.dataset.title < b.dataset.title) {
                    return -1
                } else {
                    return 0;
                }
            })
            list.map(function(elem){
                $parent.append(elem); 
            })
        });

        $(".remove-pic").click(function(e){
            $.post(document.location.pathname, 
                {action : "delete_page",page_id : this.dataset.pageId }
            ).done(function(){
                alert("Deleted");
            });
        });
        $("[value=change_order]").click(function(){
            var order = [];
            $(".sortable-thumbs .sort-thumb").each(function(i,elem){
                return order.push(elem.dataset.pageId);
            });
            $.post(document.location.pathname,
                { action : "change_order",order : order}
            ).done(function(){
                alert("Saved");
            });
        });

        //Admin Updates
        $("body").on("click","[admin-key]",function (e,target) {
            var url =  this.getAttribute("admin-key");
            var self = this;
            var value = window.prompt("url",this.dataset.value);
            if(value){
                var dValue = btoa(value);
                $.post("/api/admin/update/"+url,{
                    dvalue : dValue
                }).done(function (resp) {
                    self.dataset.value = resp.value;
                    //document.location.reload();
                });
            }
        });
        
    })


})();


(function(win){
    win.picaso = {
        link_canonical : null,
        setupDisqus : function(){
            var self  = this;
            var _DISQUS_RESET = debounce(function(){
                if (window.DISQUS) {
                    window.DISQUS.reset({
                        reload: true,
                        config: function () {
                            this.page.identifier = document.location.hash;
                            self.link_canonical = 
                                self.link_canonical || $("link[rel='canonical']").attr("href");
                            if(self.link_canonical){
                                this.page.url = self.link_canonical;
                            }
                        }
                    });
                    _DISQUS_RESET = null;
                } else {
                    setTimeout(function(){
                        if(_DISQUS_RESET) {
                             _DISQUS_RESET();
                        }
                    },200);
                }
            },1000);
            _DISQUS_RESET();


            $("body").on("click",".comments_button,.toggleComment",function(){
                $("#show_comments").toggleClass("show");
                if($("#show_comments").hasClass("show")){
                   if($("#viewer_bc video")[0]){
                        $("#viewer_front").html($("#viewer_bc").html());
                    } else {
                        $("#main_image2").attr("src",$("#main_image").attr("src"));
                    }
                    $("#show_prev2").attr("href",$("#show_prev").attr("href"));
                    $("#show_next2").attr("href",$("#show_next").attr("href"));
                }
                return
            });
        },
        setupUploader : function(){
            var total_images = 0;
            var pipe = $.dQ(function(){
                return total_images === 0;
            });
            $('.cloudinary-fileupload').each(function (i, elem) {
                var field = $(this);
                var formData = JSON.parse(elem.dataset.formData);
                var query = formData.notification_url.split("?")[1];

                $(elem).cloudinary_fileupload({
                    disableImageResize: false,
                    imageMaxWidth: 800,                           // 800 is an example value
                    imageMaxHeight: 600,                          // 600 is an example value
                    maxFileSize: 20000000,                        // 20MB is an example value
                    loadImageMaxFileSize: 20000000,               // default is 10MB
                    acceptFileTypes: /(\.|\/)(gif|jpe?g|png|bmp|ico)$/i
                }).bind('cloudinarydone', function (e, data) {
                    total_images--;
                    console.error("===cloudinarydone", this, data);
                    pipe.add($.post("/api/picaso/image/upload?"+query,{
                        imageData : data.result
                    }).done(function (resp) {
                        console.error("resp", resp);
                    }));
                    return true;
                }).bind('fileuploadadd', function (e, data) {
                    total_images++;
                    console.error("===fileuploadadd", this, e, data);
                }).bind('fileuploaddrop', function (e, data) {
                    //console.error("fileuploaddrop",e,data);
                    console.error(e, e.target, elem)
                    if (e.target !== elem) {
                        e.preventDefault();
                        return false;
                    }
                }).bind('fileuploadprogressall', function(e, data){
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    console.error("fileuploadprogressall",progress,data);
                    if(progress == 100){
                        pipe.done(function(){
                            console.error("Now Reload");
                            document.location.reload();
                        });
                        progress = 99;
                    }
                    $('#upload_progress .progress-bar').css(
                        'width',
                        progress + '%'
                    );
                    $('.progress-bg').addClass("active").css({
                        "background-size" : progress + "% 100%"
                    })
                    
                });
            });             
        }
    }
})(this);

