<?php

namespace app\controller {

    //require_once("../../lib/jublonet/codebird-php/src/codebird.php");

    use app\service\R;
    use app\service\Cloudinary;
    use app\utils\ImageUtils;
    use Codebird\Codebird;

    class PicasoJobController extends AbstractController {

        public static $TUMBLR_BLOG = "myblog.tumblr.com";
        public static $CONSUMER_KEY =  "DYkCfU8QAoykemXoRSnrOZK5hSgm1xQumflpstci5dxH0Oegty";
        public static $CONSUMER_SECRET =  "V0bRvm863Cx9gOSkd4rVtaCHc9VFDjkab5tq9hFJijlitiwQzx";
        //foruser tubmlr
        public static $OAUTH_TOKEN =  "lziqjsx82dIEBsb0B0gOvHz9ttgJnQ0NoBn12T3yZdy8dOHfVm";
        public static $OAUTH_SECRET = "Pyq0HhEkopSOR2h1FMBMeGcD904N2neWfePjhAZ6hqZVnlepGb";

        public static $TWITTER_API_KEY = 'PhqgIK3IKJTywC1uAvWK6uSSe';
        public static $TWITTER_API_SECRET = 'gvdRgeoIR7qXdJHwdj3eeyn3WzzohchvUoIOXKjSA8epRDicAb';
        //for twitter user
        public static $TWITTER_TOKEN = '273498164-DiJd7PbSqabAtpn43kC7Bd30lvc4YJprirOnZ2j8';
        public static $TWITTER_TOKEN_SECRET = 'w7YuLqGbzY0MkaNXYagMKBERq9v4TcLQPZNNhEhKOWWeB';
        public static $IMAGE_URL_FORMAT = "http://mywebsite.com/image/%s.html";

        public function _before_controller_(){
            if (class_exists('\Config')) {
                $options = \Config::get("PICASO_CONFIG");
                if (!is_null($options) && !empty($options)) {
                    //TUBMBLR
                    self::$TUMBLR_BLOG = $options["TUMBLR_BLOG"];
                    self::$CONSUMER_KEY = $options["TUMBLR_CONSUMER_KEY"];
                    self::$CONSUMER_SECRET = $options["TUMBLR_CONSUMER_SECRET"];
                    self::$OAUTH_TOKEN = $options["TUMBLR_OAUTH_TOKEN"];
                    self::$OAUTH_SECRET = $options["TUMBLR_OAUTH_SECRET"];
                    //TWITTER
                    self::$TWITTER_API_KEY = $options["TWITTER_API_KEY"];
                    self::$TWITTER_API_SECRET = $options["TWITTER_API_SECRET"];
                    self::$TWITTER_TOKEN = $options["TWITTER_TOKEN"];
                    self::$TWITTER_TOKEN_SECRET = $options["TWITTER_TOKEN_SECRET"];

                    self::$IMAGE_URL_FORMAT = $options["IMAGE_URL_FORMAT"];

                    //print_r($options);
                    //echo sprintf(self::$IMAGE_URL_FORMAT,"45");
                }
                return true;
            } else {
                return false;
            }

        }

        /**
         * @RequestMapping(url="api/picaso/db/{table}/{id}", type="json")
         * @RequestParams(true)
         */
        public function dbupdate($table=null,$id=null,$key=null,$value=null){

            $bean = R::load($table,$id);
            if(!empty($key) && !empty($value) ){
                $bean->{$key} =$value ;
                R::store($bean);
            }
            return $bean;

        }

        /**
         * @RequestMapping(url="api/picaso/cron_job_backup", type="json")
         * @RequestParams(true)
         */
        public function buildWebsite($RX_MODE_BUILD){

            $gap = (3600*24);
            $now = microtime(true);
            $yesterday = floor($now/$gap)*$gap;
            $imageurl = null;


            $images = R::getAll("SELECT image.*,user.id uid,user.penname 
                FROM image,user 
                WHERE (image.user_id = user.id) AND tumblr is NULL AND (checktime < ? OR checktime is NULL) 
                ORDER BY checktime ASC, cloud_name ASC LIMIT 0, 5", array($yesterday ));

            $imageBeans = array();
            foreach($images as $image){
                $imageurl = null;
                $log = null;
                $imagebean = R::load("image",$image["id"]);
               
                $url = null;
                $matchs = array();
                if(!empty($image["full"]) && preg_match(ImageUtils::$TUMBLR, $image["full"], $matchs)==1){
                    $imagebean->tumblr = $image["full"];
                    print("\nNAlready on tumblr\n");
                } else {
                    try {
                        $format = $image["format"];
                        $caption = empty($image["title"]) ? "" : $image["title"];
                        $album_id = empty($image["album_id"]) ? 0 : $image["album_id"];
                        $link = sprintf(self::$IMAGE_URL_FORMAT,$image["id"]);
                        if(empty($format) && !empty($image["url"]) 
                            && preg_match(ImageUtils::$CLOUDINARY, $image["url"], $matchs) == 1){
                            $format = $matchs[7];
                        } 

                        $format = empty($format) ? "jpg" : $format;
                        $url = Cloudinary::image_url($image["public_name"], array(
                                "cloud_name" => $image["cloud_name"], "format" =>$format
                        ));

                        $o_full = $image["full"];
                        $o_thumb = null; $o_small=null; $o_link=null;
                        $cloud_name = $image["cloud_name"]; $public_name = $image["public_name"];

                        if(isset($image["fullsize"])){
                            $options =  array();
                            \app\utils\ImageUtils::knowImage($o_full, $o_thumb, $o_small, $o_link, $cloud_name, $public_name, $format,$options);
                            if(isset($options[$image["fullsize"]])){
                                $url = null;
                                $o_full = $options[$image["fullsize"]];
                            }
                        }

                        if(empty($imagebean->tumblr) && $this->uploadToTumblr($url,$imageurl,$caption,$image["penname"],$link,$o_full)==true){
                            $imagebean->tumblr = $imageurl;
                            print("\nNImage Posted tumblr\n");
                        } else if(empty($imagebean->twitter) && $this->uploadToTwitter($url,$imageurl,$caption,$image["penname"],$link,$o_full)==true){
                            print("\nNImage Posted on twitter\n");
                            $imagebean->twitter = $imageurl;
                        } else {
                            print("\nNot on tumblr Not in twitter\n");
                        }
                        print("\ntumblr : ".$imagebean->tumblr);
                        print("\twitter : ".$imagebean->twitter);
                        print("\n[PIC]".$image["full"]."[/PIC]\n");
                    } catch(\Exception $e){
                        $log = $e->getMessage();
                    }
                }
                
                $imagebean->checktime = $now;
                $imagebean->log = $log;
                $imageBeans[] = $imagebean;
            }
            return R::storeAll($imageBeans);
        }

        public function uploadToTwitter($url, &$twitter_id, $caption, $tags, $link,$original=null,$try=0){
            $matchs = array();
            print("\n====================".$url."======================\n");
            if(preg_match(ImageUtils::$TWITTER_COM, $url, $matchs)==1){
                $twitter_id = $matchs[3];
              } else {
                  print("\nTrying Twitter\n");
                  Codebird::setConsumerKey(self::$TWITTER_API_KEY, 
                    self::$TWITTER_API_SECRET); // static, see README
                  $cb = Codebird::getInstance();

                  $cb->setToken(self::$TWITTER_TOKEN, self::$TWITTER_TOKEN_SECRET);

                  $mediaReply = $cb->media_upload(array(
                      'media' => trim($url)
                  ));

                  if(!is_null($mediaReply) && isset($mediaReply->media_id_string) 
                    && !empty($mediaReply->media_id_string)){
                    $twitter_mid = $mediaReply->media_id_string;
                    $reply = $cb->statuses_update(array(
                            'status' => $link,
                            "media_ids" => $mediaReply->media_id_string
                        ));
                      $entitiesMedia = $reply->entities->media[0];
                      if(isset($entitiesMedia->media_url)){
                        preg_match(ImageUtils::$TWITTER_COM, $entitiesMedia->media_url, $matchs);
                        $twitter_id = $matchs[3];
                      } else {
                         print("\nReply = ".json_encode($reply)."\n");
                         return false;
                      }

                  } else {
                    print("\nMediaReply = ".json_encode($mediaReply)."\n");
                    return false;
                  }
              }
              return true;
        }

        public function uploadToTumblr($url, &$imageurl, $caption, $tags, $link, $original=null,$try=0){
            $headers = array("Host" => "http://api.tumblr.com/", "Content-type" => "application/x-www-form-urlencoded", "Expect" => "");
            print("\n====================".$url."======================\n");

            if(empty($url)){
              if(is_null($original) || empty($original)){
                return false;
              }
              return $this->uploadToTumblr($original, $imageurl, $caption, $tags, $link);
            }

            $params = array(
                "link" => $link,
                "tags" => $tags,
                "caption" => $caption,
                "type" => "photo"
            );

            if($try==1){
                print("\nTRYING WITH DATA\n");
                $params["data"] = array(file_get_contents($url));
            } else {
                print("\nTRYING WITH SOURCE\n");
                $params["source"] = $url;
            }

            $blogname = self::$TUMBLR_BLOG;
            $this->oauth_gen("POST", "http://api.tumblr.com/v2/blog/$blogname/post", $params, $headers);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_USERAGENT, "PHP Uploader Tumblr v1.0");
            curl_setopt($ch, CURLOPT_URL, "http://api.tumblr.com/v2/blog/$blogname/post");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Authorization: " . $headers['Authorization'],
                "Content-type: " . $headers["Content-type"],
                "Expect: ")
            );

            $params = http_build_query($params);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            $response = curl_exec($ch);

            $responseObj = json_decode($response,true);
            $postid = null;

            if(floor($responseObj["meta"]["status"]/100)==2 && isset($responseObj["response"]["id"])){
                $postid = $responseObj["response"]["id"];
            } else {
                if($try==0){
                    return $this->uploadToTumblr($url, $imageurl, $caption, $tags, $link,$original,1);
                } else if(!is_null($original) && !empty($original)){
                    return $this->uploadToTumblr($original, $imageurl, $caption, $tags, $link);
                }
                print("\nUPLOAD ERROR(try:".$try."\n");
                print("\n".$response."\n");
                return false;
            }

            $ch2 = curl_init();
            curl_setopt($ch2, CURLOPT_USERAGENT, "PHP Uploader Tumblr v1.0");
            curl_setopt($ch2, CURLOPT_URL, "http://api.tumblr.com/v2/blog/$blogname/posts/photo?id=".$postid);
            curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1 );
            curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                "Authorization: " . $headers['Authorization'],
                "Content-type: " . $headers["Content-type"],
                "Expect: ")
            );
            $response2 = curl_exec($ch2);
            $responseObj2 = json_decode($response2,true);

            if(floor($responseObj2["meta"]["status"]/100)==2 && isset($responseObj2["response"]["posts"]["0"]["photos"][0]["original_size"]["url"])){
                $imageurl = $responseObj2["response"]["posts"]["0"]["photos"][0]["original_size"]["url"];
            } else {
                print("\nGET\n");
               print("\n".$response2."\n");
                return false;
            }
            return true;
        }

        /**
         */
        public function oauth_gen($method, $url, $iparams, &$headers){

            $iparams['oauth_consumer_key'] = self::$CONSUMER_KEY;
            $iparams['oauth_nonce'] = strval(time());
            $iparams['oauth_signature_method'] = 'HMAC-SHA1';
            $iparams['oauth_timestamp'] = strval(time());
            $iparams['oauth_token'] = self::$OAUTH_TOKEN;
            $iparams['oauth_version'] = '1.0';
            $iparams['oauth_signature'] = $this->oauth_sig($method, $url, $iparams);
            //print $iparams['oauth_signature'];  
            $oauth_header = array();
            foreach($iparams as $key => $value) {
                if (strpos($key, "oauth") !== false) { 
                   $oauth_header []= $key ."=".$value;
               }
           }
           $oauth_header = "OAuth ". implode(",", $oauth_header);
           $headers["Authorization"] = $oauth_header;
       }

        /**
         */
        public function oauth_sig($method, $uri, $params) {
            $parts []= $method;
            $parts []= rawurlencode($uri);

            $iparams = array();
            ksort($params);
            foreach($params as $key => $data) {
                if(is_array($data)) {
                    $count = 0;
                    foreach($data as $val) {
                        $n = $key . "[". $count . "]";
                        $iparams []= $n . "=" . rawurlencode($val);
                        $count++;
                    }
                } else {
                    $iparams[]= rawurlencode($key) . "=" .rawurlencode($data);
                }
            }
            $parts []= rawurlencode(implode("&", $iparams));
            $sig = implode("&", $parts);
            return base64_encode(hash_hmac('sha1', $sig, self::$CONSUMER_SECRET."&". self::$OAUTH_SECRET, true));
        }

        /**
         */
        public  function rrmdir($src) {
            if(is_dir( $src )){
                $dir = opendir($src);
                while(false !== ( $file = readdir($dir)) ) {
                    if (( $file != '.' ) && ( $file != '..' )) {
                        $full = $src . '/' . $file;
                        if ( is_dir($full) ) {
                            self::rrmdir($full);
                        }
                        else {
                            unlink($full);
                        }
                    }
                }
                closedir($dir);
                rmdir($src);
            }
        }
    }
}


//155067964820