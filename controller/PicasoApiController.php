<?php

namespace app\controller {

    use app\service\R;
    use app\service\Cloudinary;
    use RudraX\Utils\Webapp;
    use RudraX\RX;
    use app\utils\ImageUtils;


    class PicasoApiController extends AbstractController
    {

        /**
         * @RequestMapping(url="api/picaso/image/get",type="json",cache=true)
         * @RequestParams(true)
         */
        public function image_get($model, $id = null, $cloud_name=null, $public_name=null)
        {
            $image = null;
            if(!is_null($cloud_name) && !is_null($public_name)){
                $image = R::findOne("image","cloud_name=? AND public_name=?", array($cloud_name,$public_name));
            } else if(!is_null($id)){
                $image = R::findOne("image","id=?", array($id));
            }
            if(is_null($image) || empty($image)){
                header('HTTP/1.0 404 Not Found');
                exit();
            } return  $image;
        }

        /**
         * @RequestMapping(url="api/picaso/image/update",type="json")
         * @RequestParams(true)
         */
        public function image_update($model, $id = null, $cloud_name=null, $public_name=null, $action=null,$fullsize=null)
        {
            $image = null;
            if(!is_null($cloud_name) && !is_null($public_name)){
                $image = R::findOne("image","cloud_name=? AND public_name=?", array($cloud_name,$public_name));
            } else if(!is_null($id)){
                $image = R::findOne("image","id=?", array($id));
            }

            if(is_null($image) || empty($image)){
                header('HTTP/1.0 404 Not Found');
                exit();
            }

            if(!empty($action)){
                if($action == "fullsize"){
                    if(!empty($fullsize) && $image->fullsize!= $fullsize){
                        $image->fullsize = $fullsize;
                        $image->tumblr = null;
                        $image->twitter = null;
                        $image->checktime = null;
                        R::store($image);
                    }
                }
            }

            return  $image;
        }

        /**
         * @RequestMapping(url="api/picaso/image/details",type="json")
         * @RequestParams(true)
         */
        public function cloudinary_details($uid = null, $full = null, $thumb = "", $link = "",
                                           $album_id = NULL, $collection_id = null, $small = "",
                                           $cloud_id = null, $cloud_name = null, $format = null, $etag = null,
                                           $imageData = null, $scene_note = "", $ssid = null, $public_name = null,
                                           $linkd = "", $chapter_id = null, $id = null, $celeb_id= null,
                                           $prevent = 0, $populate = 0, $fulld = null, $sign = null
        )
        {

            $cloud_id = empty($cloud_id) ? $public_name : $cloud_id;

            if (!is_null($ssid)) {
                $user = R::findOne("user", "ssid=?", array($ssid));
                if (!is_null($user)) {
                    $uid = $user->id;
                }
            }
            if (is_null($uid)) {
                $uid = $this->user->uid;
            }

            $options = array();

            if (!empty($full)) {
                $full = addslashes(\app\utils\TextUtils::descript(strip_tags($full, \app\utils\TextUtils::$ALLOWED_TAGS)));
            } else if (!empty($fulld)) {
                $full = base64_decode($fulld);
            }
            //print_js_comment("FULL:".$full);
            if (!empty($thumb)) {
                $thumb = addslashes(\app\utils\TextUtils::descript(strip_tags($thumb, \app\utils\TextUtils::$ALLOWED_TAGS)));
            }
            if (!empty($link)) {
                $link = addslashes(\app\utils\TextUtils::descript(strip_tags($link, \app\utils\TextUtils::$ALLOWED_TAGS)));
            } else if (!empty($linkd)) {
                $link = base64_decode($linkd);
            }
            if (!empty($small)) {
                $small = addslashes(\app\utils\TextUtils::descript(strip_tags($small, \app\utils\TextUtils::$ALLOWED_TAGS)));
            }

            $sexyURL = null;
            //  "/(http|https):\/\/(.*\.sexyfrenz\.com)\/(\w+)([0-9]*)\/(.*)\/(.*)\/([\w0-9=]*)\.?(.*)/";
            if(preg_match(ImageUtils::$SEXY_FRENZ_ID, $full, $sexyURL) == 1){
                if(!empty($sexyURL[4]) && \app\service\SwimAuth::$CLOUD_CONTEXT == $sexyURL[3]){
                     $id = $sexyURL[4];
                } else {
                    $cloud_name = $sexyURL[5];
                    $cloud_id = $sexyURL[7];
                    $format = $sexyURL[8];
                }
            }
            ImageUtils::knowImage($full, $thumb, $small, $link, $cloud_name, $cloud_id, $format, $options);


            if(!empty($id)){
                $image = R::load("image", $id);
            }

            if(!empty($cloud_id) && !empty($cloud_name)){
                $image = R::findOne("image", "public_name=? AND cloud_name=?",
                    array($cloud_id,$cloud_name));
                //print_r($image);
            }

            if(empty($image) || empty($image->id)){
                if(isset($options['hash'])){
                    $sign = $options['hash'];
                    $image = R::findOne("image", "full like ?", array($sign));
                } else {
                    $image = R::findOne("image", "full = ?", array($full));
                }
            }


            if(!empty($image) && !empty($image->id)){
                if(empty($cloud_name)){
                    $cloud_name = $image->cloud_name;
                    $public_id = $image->public_id;
                    $format = $image->format;
                }
            }
            //return array('' => "=" );
            //return $options;
            //return $image;

            //print_r($options );

            if ($image == NULL || empty($image)) {
                if (isset($options["twitter_id"])) {
                    //  print_js_comment("TWITTER:".$options["twitter_id"]);
                    $image = R::findOne("image", "twitter=?", array($options["twitter_id"]));
                } else if (isset($options["tumblr_id"])) {
                    $image = R::findOne("image", "tumblr like ?", array($options["tumblr_id"] . "%"));
                }
            }
            //print_r( $image );

            //print_r(array( $cloud_id,$cloud_name,$full ));
            //SELECT * FROM `image` WHERE `tumblr` like "http://68.media.tumblr.com/ffde039440f079b6fb746b449a64eeb9/tumblr_ojbf34B7et1w1ejcco1_%"
            $imageData = null;
            if ($image == NULL || empty($image)) {
                if($prevent){
                    return array(
                        'cloud_id' => $cloud_id , 'public_name' => $public_name,
                        'cloud_name' => $cloud_name,
                        'format' => $format,
                        'full' => $full,
                        'sign' => $sign
                    );
                }
                // echo "IMAGE_IS_NULL";
                if (is_null($cloud_id) || !isset($cloud_id) || empty($cloud_id)) {
                    $imageData = Cloudinary::upload($full, array());
                    if (!is_null($imageData)) {
                        $cloud_id = $imageData ["public_id"];
                        $format = $imageData ["format"];
                        $cloud_name = Cloudinary::$CLOUD_NAME;
                    }
                } else {

                    $imageData = array(
                        "cloud_name" => $cloud_name,
                        "public_id" => $cloud_id,
                        "format" => $format
                    );
                }
            }

            $CloudinaryClassInstance = RX::handler("cloudinary");
            if (!is_null($CloudinaryClassInstance) && !$prevent) {
                $CloudinaryClassInstance->image = $image;
                $CloudinaryClassInstance->cloud_name = $cloud_name;
                $CloudinaryClassInstance->user_id = $uid;
                $CloudinaryClassInstance->album_id = $album_id;
                $CloudinaryClassInstance->chapter_id = $chapter_id;
                $CloudinaryClassInstance->collection_id = $collection_id;
                $CloudinaryClassInstance->celeb_id = $celeb_id;
                $CloudinaryClassInstance->full = $full;
                $CloudinaryClassInstance->title = $scene_note;
                $image = $CloudinaryClassInstance->image_upload_callback($imageData);
            }

            if($populate && !empty($image)){
                $image->album;
                $image->via("imagecollection")->sharedCollectionList;
                $image->sharedCelebList;
                $image->sharedChapterList;
                $image->via("imagealbum")->sharedAlbumList;
            }
            return $image;
        }

        /**
         * @RequestMapping(url="api/picaso/image/upload",type="json")
         * @RequestParams(true)
         */
        public function cloudinary_upload($imageData = null)
        {
            $CloudinaryClassInstance = RX::handler("cloudinary");
            if (!is_null($CloudinaryClassInstance)) {
                if ($this->user->isValid()) {
                    $CloudinaryClassInstance->user_id = $this->user->uid;
                } 
               return $CloudinaryClassInstance->image_upload_callback($imageData);
            }
            return array(
                "success" => false
            );
        }

        /**
         * @RequestMapping(url="api/picaso/collection/image/remove",type="json")
         * @RequestParams(true)
         */
        public function collection_image_remove($image_id = null, $collection_id=null, $celeb_id=null, $album_id=null)
        {
            if(!empty( $image_id)){
                if(!empty($collection_id)){
                    $imagecollection = R::findOne("imagecollection",
                        "image_id=? AND collection_id=?", array(
                        $image_id, $collection_id
                    ));
                   if(!empty($imagecollection)){
                        R::trash($imagecollection);
                   }
                   return $imagecollection;
                } else if(!empty($celeb_id)) {
                  $celeb_image = R::findOne("celeb_image",
                        "image_id=? AND celeb_id=?", array(
                        $image_id, $celeb_id
                    ));
                    if(!empty($celeb_image)){
                        R::trash($celeb_image);
                    }
                    return $celeb_image;
                }  else if(!empty($album_id)) {
                  $imagealbum = R::findOne("imagealbum",
                        "image_id=? AND album_id=?", array(
                        $image_id, $album_id
                    ));
                    if(!empty($imagealbum)){
                        R::trash($imagealbum);
                    }
                    return $imagealbum;
                }
            }
        }

    }
}
