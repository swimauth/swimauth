<?php

namespace app\controller {

    //require_once("../../lib/jublonet/codebird-php/src/codebird.php");

    use app\service\R;
    use app\service\Cloudinary;
    use app\utils\ImageUtils;
    use Codebird\Codebird;
    use RudraX\Utils\Webapp;

    class TwitterScrapController extends AbstractController {

        public static $TWITTER_API_KEY = 'PhqgIK3IKJTywC1uAvWK6uSSe';
        public static $TWITTER_API_SECRET = 'gvdRgeoIR7qXdJHwdj3eeyn3WzzohchvUoIOXKjSA8epRDicAb';
        //for twitter user
        public static $TWITTER_TOKEN = '273498164-DiJd7PbSqabAtpn43kC7Bd30lvc4YJprirOnZ2j8';
        public static $TWITTER_TOKEN_SECRET = 'w7YuLqGbzY0MkaNXYagMKBERq9v4TcLQPZNNhEhKOWWeB';
        public static $IMAGE_URL_FORMAT = "http://mywebsite.com/image/%s.html";

        public function _before_controller_(){
            if (class_exists('\Config')) {
                $options = \Config::get("PICASO_CONFIG");
                if (!is_null($options) && !empty($options)) {
                    //TWITTER
                    self::$TWITTER_API_KEY = $options["TWITTER_API_KEY"];
                    self::$TWITTER_API_SECRET = $options["TWITTER_API_SECRET"];
                    self::$TWITTER_TOKEN = $options["TWITTER_TOKEN"];
                    self::$TWITTER_TOKEN_SECRET = $options["TWITTER_TOKEN_SECRET"];

                    self::$IMAGE_URL_FORMAT = $options["IMAGE_URL_FORMAT"];

                    //print_r($options);
                    //echo sprintf(self::$IMAGE_URL_FORMAT,"45");
                }
                return true;
            } else {
                return false;
            }

        }

        /**
         * @RequestMapping(url="api/picaso/twitter/{username}", type="template")
         * @RequestParams(true)
         */
        public function twitter($model,$username=null,$id_str="",$value=null){

            \Codebird\Codebird::setConsumerKey(self::$TWITTER_API_KEY, self::$TWITTER_API_SECRET);
            $cb = \Codebird\Codebird::getInstance();
            $cb->setToken(self::$TWITTER_TOKEN,  self::$TWITTER_TOKEN_SECRET);
            $reply = $cb->statuses_userTimeline("count=100&screen_name=".$username.(empty($id_str) ? "" : "&max_id=".$id_str));

            //print json_encode($reply);

            $images = array();
            $newer = 0;$older = 0;
            foreach ($reply as $key => $tweet) {
                if(isset($tweet->entities->media)){
                    foreach ($tweet->entities->media as $media) {
                        $images[] =  array("url"=>$media->media_url,"hash"=>base64_encode($media->media_url));
                    }
                }
                if(empty($newer)){
                    $newer = $tweet->id_str;
                }
                if(!empty($tweet->id_str)){
                    $older = $tweet->id_str;
                }
            }
            $model->assign("images",$images);
            $model->assign("older",$older);
            $model->assign("newer",$newer);
            $model->assign("username",$username);
            $model->assign("REMOTE_HOST", Webapp::$PROTOCOL."://".Webapp::$DOMAIN);

            \app\service\Smarty::setTemplateDir("../view");
            return "twitter_image";
        }
    }
}


//155067964820