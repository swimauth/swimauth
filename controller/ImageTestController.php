<?php

namespace app\controller {

    use app\utils\ImageUtils;

    class ImageTestController extends AbstractController
    {
        public static $PATERN = "/(tse|sf|cx|temp)([0-9]*)/(.*)/(html)/(.*)\.(jpeg|png|jpg|gif|json|webp|webm|mp4)/";

        /**
         * @RequestMapping(url="swim/image/test",type="json")
         * @RequestParams(true)
         */
        public function image_view($full = null, $thumb = "", $link = "",
           $small = "", $cloud_id = null, $cloud_name = null, $format = null,
           $public_name = null,
           $linkd = "")
        {

            $cloud_id = empty($cloud_id) ? $public_name : $cloud_id;
            $options = array();
            $full = addslashes(\app\utils\TextUtils::descript(strip_tags($full, \app\utils\TextUtils::$ALLOWED_TAGS)));
            if (!empty($thumb)) {
                $thumb = addslashes(\app\utils\TextUtils::descript(strip_tags($thumb, \app\utils\TextUtils::$ALLOWED_TAGS)));
            }
            if (!empty($link)) {
                $link = addslashes(\app\utils\TextUtils::descript(strip_tags($link, \app\utils\TextUtils::$ALLOWED_TAGS)));
            } else if (!empty($linkd)) {
                $link = base64_decode($linkd);
            }
            if (!empty($small)) {
                $small = addslashes(\app\utils\TextUtils::descript(strip_tags($small, \app\utils\TextUtils::$ALLOWED_TAGS)));
            }

             //echo $link;
            ImageUtils::knowImage($full, $thumb, $small, $link, $cloud_name, $cloud_id, $format, $options);

            return array(
                "full" => $full,
                "thumb" => $thumb,
                "small" => $small,
                "link" => $link,
                "cloud_name" => $cloud_name,
                "cloud_id" => $cloud_id,
                "format" => $format,
                "options" => $options
                );
        }


        /**
         * @RequestMapping(url="swim/image/info/{siteid}/{cloud_name}/{size}/{public_name}.{format}",type="template")
         * @RequestParams(true)
         */
        public function image_view2( $model, $url = "" , $siteid = "",
            $site="",$id="",$cloud_name=false,$size="mth",$public_name="", $format="jpg",
            $nonew=true)
        {   
            $match;
            if( preg_match("/(tse|sf|cx|temp)([0-9]*)/",$siteid ,$match) == 1 ){
                $site = $match[1];
                $id = $match[2];
                $search = "";
                if(in_array($cloud_name, array("hash","cover","search","profile"))){
                    $search = base64_decode($public_name);
                }

                $model->assign("site",$site);
                $model->assign("id",$id);
                $model->assign("cloud_name",$cloud_name);
                $model->assign("size",$size);
                $model->assign("public_name",$public_name);
                $model->assign("format",$format);
                $model->assign("search",$search);
            }
            $model->assign("CLOUD_CONTEXT",\app\service\SwimAuth::$CLOUD_CONTEXT);
            \app\service\Smarty::setTemplateDir("../view");
            return "info";
        }

    }
}
