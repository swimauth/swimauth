<?php

namespace app\controller {

    use app\utils\ImageUtils;

    class SwimAuthController extends AbstractController
    {


        /**
         * @RequestMapping(url="swim/auth/login")
         * @RequestParams(true)
         */
        public function login()
        {
            $RETURN_URL = isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : false;
            $THIS_URL = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

            if(!$this->user->isValid() && !$this->user->validate()){
                $options = array();
                $options[\app\service\SwimAuth::$OTP_DEST_URL_KEY] = $RETURN_URL;
                $config = \app\service\SwimAuth::authenticate($options);
                if( method_exists($this->user,"onSwimAuth")){
                    $this->user->onSwimAuth($config);
                }
            }
            if(!empty($RETURN_URL) && $RETURN_URL!=$THIS_URL){
                header("Location: " . $RETURN_URL);
            }
        }

        /**
         * @RequestMapping(url="swim/auth/callback")
         * @RequestParams(true)
         */
        public function auth()
        {
            $config = \app\service\SwimAuth::authenticate();
            if( method_exists($this->user,"onSwimAuth")){
                $this->user->onSwimAuth($config);
            }

            $RETURN_URL = \app\service\SwimAuth::VAL(\app\service\SwimAuth::$OTP_DEST_URL_KEY);
            $THIS_URL = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            if(!empty($RETURN_URL) && $RETURN_URL!=$THIS_URL){
                header("Location: " . $RETURN_URL);
            }
        }

        /**
         * @RequestMapping(url="swim/auth/logout")
         * @RequestParams(true)
         */
        public function logout()
        {
            $RETURN_URL = \app\service\SwimAuth::VAL(\app\service\SwimAuth::$OTP_LOGOUT_URL_KEY);
            $THIS_URL = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            if(!empty($RETURN_URL) && $RETURN_URL!=$THIS_URL){
                header("Location: " . $RETURN_URL);
                die();
            } else if($this->user->isValid()){
                $this->user->setInValid();
                \app\service\SwimAuth::logout($_SERVER["HTTP_REFERER"]."?".microtime());
            }
            header("Location: /?out_".microtime());
            echo "User Logged out.";
        }

    }
}
